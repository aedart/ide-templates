# IDE Templates #

Contains various IDE templates and utilities, which aim at speeding up certain developing tasks, such as writing getters and setters.

[TOC]

## JetBrains ##

### PHPStorm ###

### Aedart\Model Templates ###

`Aedart\Model` live template group. Contains getter / setter template(s). See [JetBrains documentation](https://www.jetbrains.com/phpstorm/help/live-templates.html) for information on how to install.